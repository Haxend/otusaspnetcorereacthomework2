import React, { useState } from 'react';
import { Button, Container, CssBaseline, TextField, Typography } from '@mui/material';
import { connect } from 'react-redux';
import { loginAction } from '../Components/Actions';
import { RootState } from '../store';

//interface FormStyles {
//    paper: React.CSSProperties;
//    form: React.CSSProperties;
//    submit: React.CSSProperties;
//    testLoginInfo: React.CSSProperties;
//}

interface LoginProps {
    error: string,
    login: (username: string, password: string) => void;
}

function LoginPage({
    error,
    login
}: LoginProps) {

    const [formData, setFormData] = useState({ username: '', password: '' });

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    };

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const { username, password } = formData;
        login(username, password);
    };

    return (
        <Container component="main">
            <CssBaseline />
            <div>
                <Typography>           </Typography>
                <form onSubmit={handleSubmit}>
                    <div>
                        <TextField label="                "
                            name="username"
                            value={formData.username}
                            onChange={handleChange} />
                    </div>
                    <div>
                        <TextField label="      "
                            type="password"
                            name="password"
                            value={formData.password}
                            onChange={handleChange} />
                    </div>
                    <div>
                        <Button type="submit"
                            variant="contained"
                            color="primary">
                                 
                        </Button>
                    </div>
                </form>
            </div>
        </Container >
    );
}

const mapStateToProps = (state: RootState) => ({
    error: state.auth.error,
});

const mapDispatchToProps = {
    login: loginAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);