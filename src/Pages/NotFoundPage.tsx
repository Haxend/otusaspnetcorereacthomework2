import { Container, Typography } from '@mui/material';

const NotFound = () => {
    return (
        <Container>
            <Typography>404 Not found</Typography>
        </Container>
    );
};

export default NotFound;
